from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys
#import cv2
import os
import outputwrapper as ow
import DC_gimbal as Gimbal
import store_log as store
import numpy as np
#import DC_nogimbal as noGimbal
import matplotlib.pyplot as plt
import store_log_manual as man_log
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
#import DC_nogimbal as nogimbal






class MyApp(QTabWidget,QMainWindow,QLabel):


     # Ui_MainWindow
    def __init__(self):
        QMainWindow.__init__(self)
        QTabWidget.__init__(self)
        QLabel.__init__(self)
        self.checkpoints = 0
        self.store_log_counter = 0
        self.l_order = []
        self.hor_lat = []
        self.hor_lon = []
        self.hor_speed = []
        self.vert_alt = []
        self.vert_speed = []
        self.yaw_order=[]
    	self.written_file = ""
        self.asset_name = "Stack" #Default asset name is Stack
        self.distance = 0

        self.initUI()

    def initUI(self):
        self.setGeometry( 150, 150, 650, 500)
        #self.showFullScreen()



# **** Threads ****
        self.takeoff_thread = Takeoff()
        self.connect_thread = DroneConnect()
        self.flight_thread = Flight()
        self.manual_thread = Switch()
        self.vertical_thread = just_vertical()
        self.setWindowTitle("Drone Control")

        grid = QGridLayout()

        b1 = QPushButton("Connect")
        self.connect(b1, SIGNAL('clicked()'), self.initialise)

        #b1.clicked.connect(lambda: self.normalOutputWritten(sys.stdout))

        self.b2 = QPushButton("Enter Checkpoints")
        b1.setEnabled(True)
        self.connect(self.b2, SIGNAL('clicked()'), self.enter_checkpoint)
        #b2.addStretch()
        #self.connect(b2, SIGNAL('clicked()'), self.enter_checkpoint)
        b3 = QPushButton("Pause")
        self.connect(b3, SIGNAL('clicked()'), self.loiter_vehicle)

        b4 = QPushButton("End Flight")
        self.connect(b4, SIGNAL('clicked()'), self.land_vehicle)
        b4.setEnabled(True)

        b5 = QPushButton("Test Takeoff")
        b6 = QPushButton("Switch to Manual")
        self.b7 = QPushButton("Start Flight")
        self.connect(b5, SIGNAL('clicked()'), self.test_takeoff)
        self.connect(b6, SIGNAL('clicked()'), self.switch_to_manual)
        self.connect(self.b7, SIGNAL('clicked()'), self.start_flight)
        self.b7.setEnabled(False)

        b8 = QPushButton("Load Path")
        self.connect(b8, SIGNAL('clicked()'), self.load_path)

        self.b9 = QPushButton("Vertical Takeoff")
        self.connect(self.b9, SIGNAL('clicked()'), self.simple_vertical_takeoff)
        self.b9.setEnabled(False)

        b10 = QPushButton("Store coordinates")
        self.connect(b10, SIGNAL('clicked()'), self.run_store_log)

        b11 = QPushButton("Log Manual Flight")
        self.connect(b11, SIGNAL('clicked()'), self.manual_flight_log)

        b12 = QPushButton("Open Last Saved Log")

        # /****** Checklist before takeoff **********/

        self.c1 = QCheckBox("Is battery level above 25")
        self.c2 = QCheckBox("Does GoPro have memory card")
        self.c3 = QCheckBox("Are propellers fixed properly")
        self.c4 = QCheckBox("Is drone on level surface")
        self.c5 = QCheckBox("Is wifi connected")
        self.c6 = QCheckBox("Is GPS locked ")
        self.c7 = QCheckBox("Is live feed proper ")
        self.c8 = QCheckBox("Is GoPro date and time set ")
        self.c9 = QCheckBox("Is dry run of propellers done ")
        self.c10 = QCheckBox("Is video recording ")

        self.c1.stateChanged.connect(lambda:self.checklist())
        self.c2.stateChanged.connect(lambda:self.checklist())
        self.c3.stateChanged.connect(lambda:self.checklist())
        self.c4.stateChanged.connect(lambda:self.checklist())
        self.c5.stateChanged.connect(lambda:self.checklist())
        self.c6.stateChanged.connect(lambda:self.checklist())
        self.c7.stateChanged.connect(lambda:self.checklist())
        self.c8.stateChanged.connect(lambda:self.checklist())
        self.c9.stateChanged.connect(lambda:self.checklist())
        self.c10.stateChanged.connect(lambda:self.checklist())





        self.te = QTextEdit()
        self.te.setReadOnly(True)

        grid.addWidget(b1, 0, 0, 1, 1)
        grid.addWidget(b3, 0, 2, 1, 1)
        grid.addWidget(b4, 0, 4, 1, 1)
        grid.addWidget(self.b7,0,6,1,1)
        grid.addWidget(b10,1,4,1,1)
        grid.addWidget(b11,1,6,1,1)

        grid.addWidget(self.c1,2,0,1,1)
        grid.addWidget(self.c2,3,0,1,1)
        grid.addWidget(self.c3,4,0,1,1)
        grid.addWidget(self.c4,5,0,1,1)
        grid.addWidget(self.c5,6,0,1,1)

        grid.addWidget(self.c6,2,2,1,1)
        grid.addWidget(self.c7,3,2,1,1)
        grid.addWidget(self.c8,4,2,1,1)
        grid.addWidget(self.c9,5,2,1,1)
        grid.addWidget(self.c10,6,2,1,1)



        grid.addWidget(self.b2, 1, 0, 1, 1)
        grid.addWidget(b8,1,2,1,1)

        #grid.addWidget(self.canvas, 4, 0, 1, 3)
        #grid.addWidget(b5,5,0,1,3)
        grid.addWidget(self.b9,7,0,1,3)
        grid.addWidget(b6,8,0,1,3)
        grid.addWidget(self.te, 2, 3, 7, 5)

        self.setLayout(grid)

#*************  Store Log Processing functions *************

    def run_store_log(self):
    	self.store_log_counter += 1
    	self.te.append("\nStarting Store Log....")
    	result, filename = store.store_log(self.store_log_counter)
    	if result == 1:

    		self.te.append("\nStore log for Point %d is completed" %self.store_log_counter)
    		self.te.append("\nPoint %d coordinates are" %self.store_log_counter)
    		a = self.read_store_log(self.store_log_counter,filename)

    		if a == 1:
    			self.te.append("\nClick store log again to run for next point")

    def read_store_log(self,counter,filename):

    	#fname = "Point_"+str(counter)+".txt"
    	if counter == 1:
    		saved_file = QFileDialog.getSaveFileName(self, "Save the data in the following file",'',"Path(*.txt)")
    		self.written_file = saved_file
    	else:
    		print("File not opened")

    	fname = filename
    	print("Counter is", counter)
    	latitude,longitude,t = [],[],[]
    	if os.path.isfile(fname):
    		print("File is opened")
    		f = open(fname, 'r')
    		lines = f.readlines()[4:]
    		#latitude,longitude,t = [],[],[]
    		for line in lines:
       			a = line.split()
        		#print(len(a))
		        if(len(a)!=14):
		            print("point reached")
		            continue
		        latitude.append(float(a[12]))
		        longitude.append(float(a[13]))
		        t.append(float(a[0]))

			#print(latitude)
			#print(longitude)
			#print(t)
			length = len(t)
			flag = length/2
			#double cor_lat, cor_lon = 0.0
			for i in range(0,length):
				#print("inside for loop")
				if 9<t[i]<14:


					#print("Found a time match")
					'''
					if ((latitude[i-1] == latitude[i]) and (latitude[i] == latitude[i+1])):
						cor_lat = latitude[i]
						cor_lon = longitude[i]
						self.te.append("\nLatitude is - %f" %cor_lat)
						self.te.append("\nLongitude is - %f" %cor_lon)
					'''
					if (latitude[i-1] == latitude[i]):
						#print("Found a lat match")
						#cor_lat = latitude[i]
						#cor_lon = longitude[i]
						flag = i
						break

				else:
					continue

		#else:
			#self.te.append("File Does not Exist. Run store log again")

		print(flag,latitude[flag],longitude[flag])
		self.te.append("\nLatitude is- %.7f" %latitude[flag])
		self.te.append("\nLongitude is- %.7f" %longitude[flag])


		#if counter == 1:
		#	saved_file = QFileDialog.getSaveFileName(self, "Save the data in the following file",'',"Path(*.txt)")

		print(self.written_file)
        g = open(self.written_file,"a")
        	#g.write("****Flight Path*****")
        g.write("\n")
        g.write("\nPoint_%d Coordinates are " %counter)
        g.write("\nLatitude is- %.7f" %latitude[flag])
        g.write("\nLongitude is- %.7f" %longitude[flag])
        g.close()
        QMessageBox.information(self, "Success", "Contents written to file successfully")
        return 1


#********************* Function for pre flight checklist*******************

    def checklist(self):
        #print("Checkbox Ticked")
        if self.c1.isChecked() == True:
            if self.c2.isChecked() == True:
                if self.c3.isChecked() == True:
                    if self.c4.isChecked() == True:
                        if self.c5.isChecked() == True:
                            if self.c6.isChecked() == True:
                                if self.c7.isChecked() == True:
                                    if self.c8.isChecked() == True:
                                        if self.c9.isChecked() == True:
                                            if self.c10.isChecked() == True:
                                                self.b7.setEnabled(True)
                                                self.b9.setEnabled(True)



#**********************************************************

#*************  Function for running log on manual flight *************

    def manual_flight_log(self):
        self.te.append("\nStarting the log for manual flight")
        folder_name = str(QFileDialog.getExistingDirectory(self, "Select Directory to Save"))
        a = man_log.manual_store_log(folder_name)
        if a == 1:
            self.te.append("\nLog written successfully")
        else:
            self.te.append("\n Unable to connect to drone. Try again!")

    def simple_vertical_takeoff(self):
        veralt,ok = QInputDialog.getDouble(self,"Altitude input dialog","Enter the Altitude",0.0,-1000,1000,8)
        if ok:
            verspd,ok = QInputDialog.getDouble(self,"Speed input dialog","Enter the Speed",0.0,-1000,1000,4)
            msg = QMessageBox.question(self," ","Do you want to start the flight?" ,QMessageBox.Yes | QMessageBox.No)
            if msg == QMessageBox.Yes:
                self.vertical_thread.vert_goto(veralt,verspd)
            elif msg == QMessageBox.No:
                self.te.append("\b Takeoff Cancelled")


    def center(self):
        gui = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        gui.moveCenter(cp)
        self.move(gui.topLeft())


    def initialise(self):
        stderr.outputWritten.connect(self.handleOutput)
        Gimbal.Set_Variables()
        self.te.append("Variables Initialised")
        msg = QMessageBox.question(self," ","Do you want to connect to the drone?" ,QMessageBox.Yes | QMessageBox.No)
        if msg == QMessageBox.Yes:
                if os.stat("info.txt").st_size == 0 :
                    text, ok = QInputDialog.getText(self, 'Asset Name', 'Enter name of Asset:')
                    if ok:
                        field, ok = QInputDialog.getText(self, 'Field Name', 'Enter name of Field:')
                        if ok:
                            f = open("info.txt",'w')
                            f.write(str(text))
                            f.write("\n")
                            f.write(str(field))
                            f.close()
                            self.make_folders(field,text)
                else:
                    f = open("info.txt", 'r+')
                    a = f.read().splitlines()
                    msg = QMessageBox.question(self," ", "Asset is %s and field is %s. Is it ok" %(a[0],a[1]), QMessageBox.Yes | QMessageBox.No)
                    if msg == QMessageBox.Yes:
                        f.close()
                        self.make_folders(a[1],a[0])
                    elif msg == QMessageBox.No:
                        f.close()
                        f = open("info.txt",'w')
                        text, ok = QInputDialog.getText(self, 'Asset Name', 'Enter name of new Asset:')
                        if ok:
                            field, ok = QInputDialog.getText(self, 'Field Name', 'Enter name of Field:')
                            if ok:
                                f.write(str(text))
                                f.write("\n")
                                f.write(str(field))
                                f.close()
                                self.make_folders(field,text)


                self.connect_thread.connect_drone()

                '''
                if result == 1:
                    self.tab2.b4.setEnabled(True)
                else:
                    self.tab2.te.append("Unable to Connect to drone")
                '''
    """
    Function Description : To automatically make Flight1, Flight2, folders after connecting
    Parameters : Field Name (Reliance, Dahej, etc), Asset Name (Stack, Cabletray, etc)
    """
    def make_folders(self,field,asset):
        folder = str(field)
        asset_name = str(asset)
        if not os.path.exists(folder):
            os.mkdir(folder)
            cwd = os.getcwd()
            os.chdir(folder)
            os.mkdir(asset_name)
            os.chdir(asset_name)
            for i in range (1,10):
                flight_no = "Flight" + str(i)
                flight_folder = os.path.join(cwd,folder,asset_name,flight_no)
                os.mkdir(flight_folder)
            os.chdir(cwd)
        else:
            cwd = os.getcwd()
            os.chdir(folder)
            if os.path.isdir(asset_name):
                os.chdir(asset_name)
                if os.path.isdir("Flight9"):
                    self.te.append("All folders already made")
                else:
                    for i in range (1,10):
                        flight_no = "Flight" + str(i)
                        flight_folder = os.path.join(cwd,folder,asset_name,flight_no)
                        os.mkdir(flight_folder)
            else:
                os.mkdir(asset_name)
                os.chdir(asset_name)
                for i in range (1,10):
                    flight_no = "Flight" + str(i)
                    flight_folder = os.path.join(cwd,folder,asset_name,flight_no)
                    os.mkdir(flight_folder)
            os.chdir(cwd)

    def enter_checkpoint(self):
        #global checkpoints
        self.checkpoints = 0
        msg = QMessageBox.question(self," ","Do you want to enter number of checkpoints?" ,QMessageBox.Yes | QMessageBox.No)
        if msg == QMessageBox.Yes:
            num,ok = QInputDialog.getInt(self,"integer input dualog","enter a number")
            if ok:
                self.checkpoints = int(num)
                QMessageBox.information(self, "Checkpoints", "Number of checkpoints is: %d" %self.checkpoints)
            #self.fbox_le.setEnabled(True)
            #b1.setEnabled(True)
        else:
            QMessageBox.information(self, "Checkpoints", "Drone is not ready")

        msg = QMessageBox.question(self," ","Do you want to enter the data?" ,QMessageBox.Yes | QMessageBox.No)
        if msg == QMessageBox.Yes:
            self.enter_path_data()
        else:
            QMessageBox.warning(self, "Data", "No Data entered")

    def enter_path_data(self):
        print("Welcome to path data function")
        i = 0
        modes = ("Horizontal Goto", "Vertical Goto")
        '''
        global l_order
        global hor_lat
        global hor_lon
        global hor_speed
        global vert_alt
        global vert_speed
        l_order = []
        hor_lat = []
        hor_lon = []
        hor_speed = []
        vert_alt = []
        vert_speed = []
        '''
        horlat = 0
        horlon = 0
        veralt = 0
        horspd = 0
        verspd = 0
        self.hor_lat.append(horlat)
        self.hor_lon.append(horlon)
        self.vert_speed.append(0)
        self.hor_speed.append(0)
        self.l_order.append('Ground Position')
        self.vert_alt.append(veralt)
        self.yaw_order.append(0)
        for i in  range(1,self.checkpoints+1):
            print(i)
            pos, ok = QInputDialog.getItem(self, "Select type of mode", "Operation %d is" %i ,modes, 0, False)
            if ok:
            	#yaw,ok = QInputDialog.getInt(self,"Yaw input dialog","Enter the yaw in degrees from -360 to +360",0,-360,360,4)
            	#if ok:
            	#	self.yaw_order.append(yaw)

                QMessageBox.information(self, "Checkpoints", "Get to Checkpoint by: %s" %pos)
                if pos == "Horizontal Goto":
                    self.l_order.append('Horizontal')
                    self.vert_alt.append(veralt)
                    self.vert_speed.append(verspd)
                    horlat,ok = QInputDialog.getDouble(self,"Latitude input dialog","Enter the Latitude",0.0,-1000,1000,7)
                    if ok:
                        self.hor_lat.append(horlat)
                    horlon,ok = QInputDialog.getDouble(self,"Longtitude input dialog","Enter the Longtitude",0.0,-1000,1000,7)
                    if ok:
                        self.hor_lon.append(horlon)
                    horspd,ok = QInputDialog.getDouble(self,"Speed input dialog","Enter the Speed",0.0,-1000,1000,4)
                    if ok:
                        self.hor_speed.append(horspd)

                elif pos == "Vertical Goto":
                    self.l_order.append('Vertical')
                    self.hor_lat.append(horlat)
                    self.hor_lon.append(horlon)
                    self.hor_speed.append(horspd)
                    veralt,ok = QInputDialog.getDouble(self,"Altitude input dialog","Enter the Altitude",0.0,-1000,1000,7)
                    if ok:
                        self.vert_alt.append(veralt)
                    verspd,ok = QInputDialog.getDouble(self,"Speed input dialog","Enter the Speed",0.0,-1000,1000,4)
                    if ok:
                        self.vert_speed.append(verspd)

                yaw,ok = QInputDialog.getInt(self,"Yaw input dialog","Enter the direction to turn after checkpoint from 0 to +360",0,0,360,4)
            	if ok:
            		self.yaw_order.append(yaw)


            self.te.append("Get to checkpoint %d by%s" %(i,self.l_order[i]))
            i +=1

        self.te.append("Done.....Data Entered....")
        #b2.setEnabled(False)

        print(self.l_order)
        print(self.yaw_order)
        print(self.vert_speed)
        print(self.vert_alt)
        #print(self.hor_lon)

        '''
        msg = QMessageBox.question(self," ","Do you want to plot the data?" ,QMessageBox.Yes | QMessageBox.No)
        if msg == QMessageBox.Yes:
            self.plot_path()
        if msg == QMessageBox.No:
        '''
        QMessageBox.information(self, "Data Entered", "Done !!")

        msg = QMessageBox.question(self," ","Do you want to save the path?" ,QMessageBox.Save | QMessageBox.No)
        if msg == QMessageBox.Save:
            self.save_path_data()
        if msg == QMessageBox.No:
            QMessageBox.information(self, "Path not saved", "Done !!")


    def plot_path(self):
        print("Printing only ho path")
        #self.figure = figure(figsize = (15,5))
        #canvas = Figurecanvas(self.figure)
        ax = self.figure.add_subplot(111)
        x = [self.hor_lat[i] for i in range(0, checkpoints)]
        y = [self.hor_lon[i] for i in range(0, checkpoints)]
        x.append(0)
        y.append(0)

        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))

        #self.tab2.te.append(x)
        #self.tab2.te.append(y)
        ax.plot(x,y)
        ax.set_title("Path Plot")
        self.canvas.draw()

        #msg = QMessageBox.question(self," ","Do you want to set the variables?" ,QMessageBox.Yes | QMessageBox.No)
        #if msg == QMessageBox.Yes:
        #    self.initialise()

    def save_path_data(self):
        self.te.append("Saving the entered path")
        saved_file = QFileDialog.getSaveFileName(self, "Save the path",'',"Path(*.txt)")
        g = open(saved_file,"a")
        g.write("****Flight Path*****")
        g.write("\n")
        #g.write("Order of Checkpoints is:")
        #g.write(l_order)
        g.write("Order of Checkpoints is:\n")
        for i in range(0, self.checkpoints+1):
            g.write(self.l_order[i])
            g.write(",")
        g.write("\n")
        g.write("Latitude Array is:\n")
        for i in range(0, self.checkpoints+1):
            g.write(str(self.hor_lat[i]))
            g.write(",")

        g.write("\n")
        g.write("Longitude Array is:\n")
        for i in range(0, self.checkpoints+1):
            g.write(str(self.hor_lon[i]))
            g.write(",")

        g.write("\n")
        g.write("Hor Speed Array is:\n")
        for i in range(0, self.checkpoints+1):
            g.write(str(self.hor_speed[i]))
            g.write(",")

        g.write("\n")
        g.write("Vertical Altitude Array is:\n")
        for i in range(0, self.checkpoints+1):
            g.write(str(self.vert_alt[i]))
            g.write(",")

        g.write("\n")
        g.write("Vertical Speed Array is:\n")
        for i in range(0, self.checkpoints+1):
            g.write(str(self.vert_speed[i]))
            g.write(",")

        g.write("\n")
        g.write("Yaw is:\n")
        for i in range(0, self.checkpoints+1):
            g.write(str(self.yaw_order[i]))
            g.write(",")

        g.close()

    def load_path(self):
        self.te.append("loading the path")
        path_filename= QFileDialog.getOpenFileName(self, "Select a path file",'','Path(*.txt)')

        if path_filename == " ":
        	print("File not selected")
        else:
	        self.te.append("Path selected is:%s" %path_filename)

	        with open(path_filename,'r') as f:
	            rand1 = f.readline()

	            rand2 = f.readline()
	            self.l_order = f.readline().split(',')
	            self.l_order = [(self.l_order[i]) for i in range(len(self.l_order) - 1)]
	            self.checkpoints = len(self.l_order) - 1
	            print("Checkpoints in file is:", self.checkpoints)

	            rand3 = f.readline()
	            self.hor_lat = (f.readline().split(','))
	            self.hor_lat = [float(self.hor_lat[i]) for i in range(len(self.hor_lat) - 1)]

	            rand3 = f.readline()
	            self.hor_lon = (f.readline().split(','))
	            self.hor_lon = [float(self.hor_lon[i]) for i in range(len(self.hor_lon) - 1)]

	            rand3 = f.readline()
	            self.hor_speed = (f.readline().split(','))
	            self.hor_speed = [float(self.hor_speed[i]) for i in range(len(self.hor_speed) - 1)]

	            rand3 = f.readline()
	            self.vert_alt = (f.readline().split(','))
	            self.vert_alt = [float(self.vert_alt[i]) for i in range(len(self.vert_alt) - 1)]

	            rand3 = f.readline()
	            self.vert_speed = (f.readline().split(','))
	            self.vert_speed = [float(self.vert_speed[i]) for i in range(len(self.vert_speed) - 1)]

	            rand2 = f.readline()
	            self.yaw_order = f.readline().split(',')
	            self.yaw_order = [int(self.yaw_order[i]) for i in range(len(self.yaw_order) - 1)]

		if len(self.yaw_order) == 0:
		  	for i in range(len(self.l_order)):
		  			self.yaw_order.append(0)


        print("Data from file")
        print("Order of mission is",self.l_order)
        print("Yaw data is",self.yaw_order)
        print("Latitude data is",self.hor_lat)
        print("Longitude data is",self.hor_lon)
        print("Hor speed  is",self.hor_speed)
        print("Vertical alt is",self.vert_alt)
        print("Vertical speed is",self.vert_speed)



    def start_flight(self):
        msg = QMessageBox.question(self," ","Do you want to start the flight?" ,QMessageBox.Yes | QMessageBox.No)
        if msg == QMessageBox.Yes:
            self.te.append("Starting the Flight")
            print("STarting ")
            print(self.checkpoints)
            self.flight_thread.actual_flight(self.checkpoints,self.l_order,self.hor_lat,self.hor_lon,self.hor_speed,self.vert_alt,self.vert_speed,self.yaw_order)


        #Gimbal.global_timer = 0




        #for idx,val in enumerate(l_order):
            #print(idx,val)

    def test_takeoff(self):
        num,ok = QInputDialog.getDouble(self,"Altitude Input","Enter the target Altitude")
        if ok:

            targ_alt = int(num)
            self.takeoff_thread.arm_check(targ_alt)
        #Gimbal.arm_and_takeoff(targ_alt)

    def land_vehicle(self):
        self.manual_thread.switchoption(3)
        #self.tab2.te.append("Landing the Drone")
        #Gimbal.Vehicle_Land()

    def switch_to_manual(self):
        self.manual_thread.switchoption(1)
        #Gimbal.abort = 0
        #Gimbal.check = 1

    def loiter_vehicle(self):
        self.manual_thread.switchoption(2)
        #Gimbal.Vehicle_loiter()





    def handleOutput(self,text,stdout):
        self.te.append(text)
    '''
    def handleOutput_2(self,text,stdout):
        self.tab2.te.append(text)
    def handleOutput_3(self,text,stdout):
        self.tab3.te.append(text)
    '''

#   ****************  THREAD FOR VERTICAL TAKEOFF **********************
class just_vertical(QThread):

    def __init__(self, parent = None):

        QThread.__init__(self, parent)
        self.exiting = False
        self.altitude = 0

    def __del__(self):

        self.exiting = True
        self.wait()

    def vert_goto(self,alt,spd):
        self.altitude = alt
        self.speed = spd
        self.start()

    def run(self):
        #result = Gimbal.arm_and_takeoff(self.altitude)
        init_time = Gimbal.get_time()
        a = Gimbal.arm_and_takeoff(4,init_time)
        dist = self.altitude-5
        Gimbal.setting_yaw()
        Gimbal.condition_yaw(0,abort,True)
        Gimbal.Vertical_goto(self.altitude,self.speed,dist,init_time,abort)
        Gimbal.Vehicle_Land(self.altitude,init_time)
            #self.tab2.te.append("Test Take Off completed successfully")





# ******************* Thread to Switch to Manual or change to Manual Mode or Pause Flight or End flight ********

'''
1. Switch to Manual (Equal to KeyboardInterrupt)
3. Pause Flight
4. End Flight


'''
class Switch(QThread):
    def __init__(self, parent = None):

        QThread.__init__(self, parent)
        self.exiting = False

    def __del__(self):

        self.exiting = True
        self.wait()

    def switchoption(self,option):
        self.option = option
        self.start()

    def run(self):
        if self.option == 1:
            print("KeyboardInterrupt to Manual")
            abort = 0
            check = 1
            Gimbal.Vehicle_loiter()
        elif self.option == 2:
            print("Pausing Flight")
            Gimbal.Vehicle_loiter()
        elif self.option == 3:
            print("Landing Flight")
            Gimbal.Vehicle_Land_non_log()







#   ****************  THREAD FOR TAKEOFF **********************
class Takeoff(QThread):

    def __init__(self, parent = None):

        QThread.__init__(self, parent)
        self.exiting = False
        self.altitude = 0

    def __del__(self):

        self.exiting = True
        self.wait()

    def arm_check(self,alt):
        self.altitude = alt
        self.start()

    def run(self):
        init_time = Gimbal.get_time()
        result = Gimbal.arm_and_takeoff(self.altitude,init_time)
        if result == 1:
            print("Test takeoff successful")
            #self.tab2.te.append("Test Take Off completed successfully")

# ************** THREAD FOR CONNECTING TO DRONE ******************

class DroneConnect(QThread):
    def __init__(self, parent = None):

        QThread.__init__(self, parent)
        self.exiting = False

    def __del__(self):

        self.exiting = True
        self.wait()

    def connect_drone(self):
        self.start()

    def run(self):
        result = Gimbal.Noctua_initialise()
        if result == 1:

            print("Connected to Drone succesfully")
        else:
            print("Unable to connect to Drone")

        if Gimbal.check_gps == 1:
            print("No GPS ! Changing to Manual Mode")
            Gimbal.Manual_mode()
        else:
            print("GPS Exists ! Continuing in Fly mode")

# *************************** THREAD FOR ACTUAL FLIGHT *******************
class Flight(QThread):
    def __init__(self, parent = None):

        QThread.__init__(self, parent)
        self.exiting = False



    def __del__(self):

        self.exiting = True
        self.wait()

    def actual_flight(self, checkpoints,order,lat,lon,horsp,alt,verspd,yaw):
        self.no_checkp = checkpoints
        self.l_order = order
        self.hor_lat = lat
        self.hor_lon = lon
        self.hor_speed = horsp
        self.vert_alt = alt
        self.vert_speed = verspd
        self.yaw = yaw
        self.start()

    def run(self):
        Gimbal.global_timer = 0
        print("No of checkpoints is", self.no_checkp)
        print("******Inside flight thread*********")
        print(self.l_order,self.yaw,self.vert_alt)

        initialize_time = Gimbal.get_time()
        a = Gimbal.arm_and_takeoff(4,initialize_time)
        print("result is -", a)
        Gimbal.setting_yaw()
        Gimbal.condition_yaw(0,abort,True)

        height = 5
        print(self.l_order)
        while abort == 1:
            #Gimbal.condition_yaw(0,abort,True)
            for i in range(1, self.no_checkp+1):
                init_time = Gimbal.get_time()
                #Gimbal.condition_yaw(90,abort,True)
                if self.l_order[i] == "Horizontal":
                    if i == 1:
                        height = 5

                    else:
                        if self.vert_alt[i] == 0:
                            height = 5
                        else:
                            height = self.vert_alt[i]
                    Gimbal.Horizontal_goto(self.hor_lat[i],self.hor_lon[i],height,self.hor_speed[i],init_time,abort)
                    Gimbal.condition_yaw(self.yaw[i],abort,True)
                elif self.l_order[i] == "Vertical":
                    height = self.vert_alt[i]
                    #if i == 1:
                     #   dist = 5
                    if (self.vert_alt[i] - self.vert_alt[i-1])<0:
                        dist = self.vert_alt[i-1] - self.vert_alt[i]
                    elif (self.vert_alt[i] - self.vert_alt[i-1])>0:
                        dist = self.vert_alt[i] - self.vert_alt[i-1]
                    elif (self.vert_alt[i] - self.vert_alt[i-1])== 0:
                        dist = 0

                    print("Distance is:", dist)
                    Gimbal.Vertical_goto(self.vert_alt[i],self.vert_speed[i],dist,init_time,abort)
                    Gimbal.condition_yaw(self.yaw[i],abort,True)


                i += 1


                '''
                if i == 2:
                    print("Checkpoint is:",i)
                    continue
                Gimbal.condition_yaw(270,abort,True)
                '''

                print("Checkpoint is:",i)

                #lat,lon,alt = Gimbal.real_time_plot()
                '''
                print("Returned data is:")
                print(lat)
                print(lon)
                print(alt)
                '''

            print("Outside Loop")
            #final_alt = self.vert_alt[self.no_checkp]
            #print(self.vert_alt, self.no_checkp)
            print("Final alt is",self.vert_alt[self.no_checkp - 1])
            #land_time = Gimabl.get_time()
            Gimbal.Vehicle_Land(5,initialize_time)
            break



if __name__ == "__main__":
    app = QApplication(sys.argv)
    original_path = os.getcwd()
    global stdout
    global stderr
    stdout = ow.OutputWrapper(True)
    stderr = ow.OutputWrapper(True)
    global abort,check
    abort = 1
    check = 0
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
