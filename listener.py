from dronekit import connect, VehicleMode, LocationGlobalRelative
import time


def Noctua_initialise():
	global vehicle

	print ("Connecting to Solo ...")
	try:
		vehicle = connect('udpin:0.0.0.0:14550', wait_ready= False)
		# @vehicle.on_message('*')
		# def l(self, name, message):
		# 	print("--",name, message)
		vehicle.mode = VehicleMode("GUIDED")
		time.sleep(1)

	# printing the initial data
		print (vehicle.attitude)
		print (vehicle.battery)
		time.sleep(2.5)

		print ("Global Relative Frame Location")
		print (vehicle.location.global_relative_frame)
		return 1

	except:
		print("Unable to connect to Solo. Check Wifi Conenction")
		return 0


def attitude_callback(self, attr_name, value):
		print ("Attitude of yaw: ", value)


	 # Add a callback `location_callback` for the `global_frame` attribute.


print("Connecting...........Solo")
num=Noctua_initialise()
if(num==1):
	print("drone is connected.")

else :
	print("not connected.")

vehicle.add_attribute_listener('backend.vehicle.attitude.yaw', attitude_callback)
# print("outside add attribute listener")
 # Wait 2s so callback can be notified before the observer is removed
time.sleep(2)
print("after time.")
 # Remove observer - specifying the attribute and previously registered callback function
vehicle.remove_message_listener('backend.vehicle.attitude.yaw', attitude_callback)
print("outside msg remove")
