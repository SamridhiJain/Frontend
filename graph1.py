import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

style.use('fivethirtyeight')

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

def read_content( filename):
	x_longitude = []
	y_latitude = []
	z_altitude = []
	with open(filename,'r') as f :
		lines = f.readlines()[4:]
		for s in lines:
			a = s.split()
			if len(a) !=14:
				print("point reached")
				continue
			x_longitude.append(float(a[12]))
			y_latitude.append(float(a[13]))
			z_altitude.append(float(a[2]))
	return x_longitude, y_latitude, z_altitude
def animate(i):
	filename = 'Attitude_data_Manual_1232018_1158.txt'
	# lines = filename.readlines()
	x_longitude = []
	y_latitude = []
	z_altitude = []

	read_content(filename)
	xs , ys , zs = x_longitude, y_latitude, z_altitude
# def animate(i):
# 	filename = open('samplefile.txt','r').read()
# 	lines = filename.split('\n')
# 	xs = []
# 	ys =[]
# 	for line in lines:
# 		if len (line)>1:
# 			x,y = line.split(',')
# 			xs.append(x)
# 			ys.append(y)
	with open(filename,'r') as f :
		lines = f.readlines()[4:]
		for line in lines:
			a = line.split()
			if len(a) !=14:
				print("point reached")
				continue
			x_longitude.append(float(a[12]))
			y_latitude.append(float(a[13]))
			z_altitude.append(float(a[2]))
	ax1.clear()
	ax1.plot(x_longitude,y_latitude)
	# ax1.plot(xs,ys)

ani= animation.FuncAnimation(fig,animate,interval=500)
plt.show()