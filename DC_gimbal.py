	#................. starting the code.................................#

# importing the required libraries for running the code
from dronekit import connect, VehicleMode, LocationGlobalRelative
import time
import math
import threading
import datetime
# import matplotlib.pyplot as plt
from pymavlink import mavutil
from numpy import pi

#.............safe algorithms........................................#

# Gets a single character from standard input.  Does not
# echo to the screen.


class _Getch:
	def __init__(self):
		try:
			self.impl = _GetchWindows()
		except ImportError:
			self.impl = _GetchUnix()
	def __call__(self): return self.impl()

class _GetchUnix:
	def __init__(self):
		import tty, sys

	def __call__(self):
		import sys, tty, termios
		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch

class _GetchWindows:
	def __init__(self):
		import msvcrt
	def __call__(self):
		import msvcrt
		return msvcrt.getch()

# Base class for other exceptions
class Interrupts(Exception):
  pass

# Raised when the there is a keyboard interrupt for to manual mode

#global abort,check

class Manual(Interrupts):
  pass

def autoToManualSwitch():
  #vehicle.channels.overrides = {}

  global abort,check
  abort = 0
  check = 1

def background(t):
	while time.time()-t < 2000:
		# print("te")
		try:
		#print("Dfg")
			interrupt=getch()
			if interrupt == "m" or interrupt== "M":
				raise Manual
			elif KeyboardInterrupt:
				autoToManualSwitch()
				exit()
			else:
				print("Try another key...")
				pass

		except Manual:
		  	print("A keyboard interrupt for switching to manual mode \n Here you go \n Handle with care :)\n")
		  	abort = 0
		  	autoToManualSwitch()
		  #Vehicle_Land()

class inputThread (threading.Thread):
  def __init__(self):
	  threading.Thread.__init__(self)
	  self.intime=time.time()

  def run(self):
	  background(self.intime)

getch=_Getch()

inthread = inputThread()
inthread.start()

#.............Run Time Code Begin.....................................#
global base_lat, base_lon
# Arms vehicle and fly to aTargetAltitude.
def arm_and_takeoff(aTargetAltitude,initial_time):
	print ("Basic pre-arm checks")
	# Don't try to arm until autopilot is ready
	# while not vehicle.is_armable:
	#     print " Waiting for vehicle to initialise..."
	#     time.sleep(1)
	print ("Arming motors")
	# Copter should arm in GUIDED mode
	#vehicle.mode    = VehicleMode("STABILIZE")
	global_timer = 0
	start_time = time.time()
	vehicle.mode    = VehicleMode("GUIDED")
	print("vehicle mode is", vehicle.mode)
	vehicle.armed   = True
	# Confirm vehicle armed before attempting to take off
	print("vehicle armed is", vehicle.armed)
	base_lat = vehicle.location.global_relative_frame.lat
	base_lon = vehicle.location.global_relative_frame.lon
	print("Base coordinates are", base_lat, base_lon)

	# ******* Writing the Data in the log file ****
	g = open(File_name,"a")
	g.write("\t \t \t \t \t--------------- ATTITUDE LOG -----------------")
	g.write("\n\n")
	g.write("Time")
	g.write("\t")
	g.write("CH3")
	g.write("\t")
	g.write("Alt")
	g.write("\t")
	g.write("CH2")
	g.write("\t")
	g.write("Pitch")
	g.write("\t")
	g.write("CH1")
	g.write("\t")
	g.write("Roll")
	g.write("\t")
	g.write("CH4")
	g.write("\t")
	g.write("Yaw")
	g.write("\t\t")
	g.write("V")
	g.write("\t")
	g.write("I")
	g.write("\t")
	g.write("%")
	g.write("\t")
	g.write("Lat")
	g.write("\t \t")
	g.write("Lon")
	g.write("\t \t")
	g.write("Dist")
	g.write("\n\n")
	g.close()
	while not vehicle.armed:
		print (" Waiting for arming...")
		time.sleep(1)
	print ("Taking off!")
	# Take off to target altitude
	vehicle.simple_takeoff(aTargetAltitude)

	speed = 1
	dur = aTargetAltitude/ speed
	dur = int(dur) + 10
	dur = float(dur)
	time_elapsed_new = InFlight(dur, initial_time, abort)
	Record_Data_final(File_name, time_elapsed_new, vehicle, abort)
	print("Now vehicle armed status is", vehicle.armed)
	# time.sleep(10)
	# Wait until the vehicle reaches a safe height before processing
	# the goto (otherwise the command after Vehicle.immediately).
	'''
	while True:
		#print("Inside while true")
		#mprint " Altitude: ", vehicle.location.global_relative_frame.alt
		#Break and return from function just below target altitude.
		print("inside while")
		if vehicle.location.global_relative_frame.alt>=aTargetAltitude*0.95:
			print("after reaching alt vehicle armed status is", vehicle.armed)
			print "Reached target altitude"
			time.sleep(5)
			break
		#else:
		#	return 0
	'''
	while True:

		#print("inside while", start_time, time.time())
		if vehicle.location.global_relative_frame.alt>=aTargetAltitude*0.95:
			print("after reaching alt vehicle armed status is", vehicle.armed)
			print("Reached target altitude")
			time.sleep(5)
			break

		elif (time.time() - start_time) > 5:
			print("Global timer loop")
			condition_yaw(0,abort,True)
			break


	return 1

def condition_yaw(heading, abort, relative = True):
	"""
	Send MAV_CMD_CONDITION_YAW message to point vehicle at a
	specified heading (in degrees).

	This method sets an absolute heading by default, but you
	can set the `relative` parameter to `True` to set yaw
	relative to the current yaw heading.

	"""
	global vehicle

	if heading != 0:
		sign = abs(heading)/heading
	else:
		sign = 1

	if abort==1:
		if relative:
			is_relative = 1 #yaw relative to direction of travel
		else:
			is_relative = 0 #yaw is an absolute angle
		# create the CONDITION_YAW command using command_long_encode()
		msg = vehicle.message_factory.command_long_encode(
			0, 0,    # target system, target component
			mavutil.mavlink.MAV_CMD_CONDITION_YAW, #command
			0, #confirmation
			heading,    # param 1, yaw in degrees
			0,          # param 2, yaw speed deg/s
			sign,          # param 3, direction -1 ccw, 1 cw
			is_relative, # param 4, relative offset 1, absolute angle 0
			0, 0, 0)    # param 5 ~ 7 not used
		# send command to vehicle
		vehicle.send_mavlink(msg)
		time.sleep(5)

def Noctua_initialise():
	global vehicle

	print ("Connecting to Solo ...")
	try:
		vehicle = connect('udpin:0.0.0.0:14550', wait_ready= False)
		# @vehicle.on_message('*')
		# def l(self, name, message):
		# 	print("--",name, message)
		vehicle.mode = VehicleMode("GUIDED")
		time.sleep(1)

	# printing the initial data
		print (vehicle.attitude)
		print (vehicle.battery)
		time.sleep(2.5)

		print ("Global Relative Frame Location")
		print (vehicle.location.global_relative_frame)
		# return 1

	except:
		print("Unable to connect to Solo. Check Wifi Conenction")
		# return 0

def Set_Variables():
	print(" Set Variables Function")
	global File_name, Req_yaw, Abort_Battery_Limit, abort, check, check_gps,initialize_time,global_timer,init_time,counter
	counter = 0
	File_name = "Attitude_" + str(datetime.datetime.now().day)+str(datetime.datetime.now().month)+str(datetime.datetime.now().year)+"_"+str(datetime.datetime.now().hour)+str(datetime.datetime.now().minute)+".txt"
	Req_yaw = 180
	Abort_Battery_Limit = 25
	abort = 1
	check = 0
	check_gps = 0
	initialize_time = time.time()
	init_time = time.time()
	global_timer = 0
	return abort, File_name

def print_roll():
	Noctua_initialise()

	global vehicle
	print("inside")
	# global counter
	# print("Cnt", counter)
	# counter+=1
	#counter+=1

	# def attitude_callback(self, attr_name, value):
#   	global last_attitude_cache
	while True:
		Pitch = vehicle.attitude.pitch
		Pitch = Pitch*180/pi
		Pitch = round(Pitch,3)
		Roll = vehicle.attitude.roll
		Roll = Roll*180/pi
		Roll = round(Roll,3)
		Yaw = vehicle.attitude.yaw
		Yaw = Yaw*180/pi
		Yaw = round(Yaw,3)
		print("pitch", Pitch)
		print("roll", Roll)
		print(Yaw)
		time.sleep(0.5)

def logfile_heading(File_name):
	g = open(File_name,"a")
	g.write("\t \t \t \t \t--------------- ATTITUDE LOG -----------------")
	g.write("\n\n")
	g.write("Time")
	g.write("\t")
	g.write("CH3")
	g.write("\t")
	g.write("Alt")
	g.write("\t")
	g.write("CH2")
	g.write("\t")
	g.write("Pitch")
	g.write("\t")
	g.write("CH1")
	g.write("\t")
	g.write("Roll")
	g.write("\t")
	g.write("CH4")
	g.write("\t")
	g.write("Yaw")
	g.write("\t\t")
	g.write("V")
	g.write("\t")
	g.write("I")
	g.write("\t")
	g.write("%")
	g.write("\t")
	g.write("Lat")
	g.write("\t \t")
	g.write("Lon")
	g.write("\t \t")
	g.write("Dist")
	g.write("\n\n")
	g.close()

def Record_Data(File_name, vehicle, abort):
	global global_timer, initialize_time, check_gps

	data = vehicle.location.global_relative_frame
	time_elapsed = time.time() - init_time
	global_timer = time.time() - initialize_time
	systemstatus = vehicle.system_status

	print(vehicle.system_status)
	print(vehicle.battery.level)
	print (data.alt)
	print ("\n")

	#real_time_plot(data.lat,data.lon,data.alt)

	if systemstatus=="CRITICAL":
		abort = 0
		check_gps = 1
	'''
	Yaw = vehicle.attitude.yaw
	Pitch  = vehicle.attitude.pitch
	Roll = vehicle.attitude.roll
	'''
	# Acquiring the required data into the filename.txt

	#init_altitude = vehicle.location.global_relative_frame.alt
	location = vehicle.location.global_relative_frame

    #altitude = location.alt - init_altitude
	altitude = location.alt
	altitude = round(altitude,3)
	longitude = location.lon
	latitude = location.lat

	Pitch = vehicle.attitude.pitch
	Pitch = Pitch*180/pi
	Pitch = round(Pitch,3)
	Roll = vehicle.attitude.roll
	Roll = Roll*180/pi
	Roll = round(Roll,3)
	Yaw = vehicle.attitude.yaw
	Yaw = Yaw*180/pi
	Yaw = round(Yaw,3)
	# print (Pitch)
	# print (Roll)
	# print(Yaw)
	g = open(File_name,"a")
	g.write(str(round(time.time()-init_time,3)))
	g.write("\t")
	g.write(str(vehicle.channels[3]))
	g.write("\t")
	g.write(str(altitude))
	g.write("\t")
	g.write(str(vehicle.channels[2]))
	g.write("\t")
	g.write(str(Pitch))
	g.write("\t")
	g.write(str(vehicle.channels[1]))
	g.write("\t")
	g.write(str(Roll))
	g.write("\t")
	g.write(str(vehicle.channels[4]))
	g.write("\t")
	g.write(str(Yaw))
	g.write("\t")
	g.write(str(vehicle.battery.voltage))
	g.write("\t")
	g.write(str(vehicle.battery.current))
	g.write("\t")
	g.write(str(vehicle.battery.level))
	g.write("\t")
	g.write(str(latitude))
	g.write("\t")
	g.write(str(longitude))
	g.write("\n")
	g.close()
	# return Pitch, Roll,Yaw, time_elapsed

def Record_Data_final(File_name,time_elapsed, vehicle, abort):
	g = open(File_name,"a")
	g.write(str(time_elapsed))
	g.write("\n\n\n\t Check Point Point Reached \n\n\n")
	g.close()

def InFlight(dur, initial_time,abort):
	global vehicle
	initial_time_inner = time.time()
	while time.time() - initial_time_inner <= dur and abort == 1:
		print (abort)

		if vehicle.battery.level <= Abort_Battery_Limit:
			print ("Battery Low... Aborting ...")
			abort = 0

		tm_elpsd = Record_Data(File_name, vehicle, abort)

		time.sleep(0.25)
	return tm_elpsd

def Vertical_goto(req_alt, speed, dist_cal, initial_time, abort):
	global vehicle
	initial = vehicle.location.global_relative_frame
	point = LocationGlobalRelative(initial.lat,initial.lon, req_alt)
	vehicle.simple_goto(point,groundspeed = speed)
	dur = dist_cal / speed
	dur = int(dur) + 10
	dur = float(dur)
	time_elapsed_new = InFlight(dur, initial_time, abort)
	Record_Data_final(File_name, time_elapsed_new, vehicle, abort)

def Horizontal_goto(req_lat, req_lon, alt, speed, initial_time, abort):
	global vehicle
	initial = vehicle.location.global_relative_frame
	point = LocationGlobalRelative(req_lat,req_lon, alt)
	vehicle.simple_goto(point,groundspeed = speed)
	dlat = point.lat - initial.lat
	dlong = point.lon - initial.lon
	dist = math.sqrt((dlat*dlat) + (dlong*dlong)) * 1.113195e5
	dur = dist / speed
	dur = int(dur) + 10
	dur = float(dur)
	time_elapsed_new = InFlight(dur, initial_time, abort)
	Record_Data_final(File_name, time_elapsed_new, vehicle,abort)

def Vehicle_Land(flight_alt, init_time):
	global vehicle
	print ("Landing...")
	vehicle.mode = VehicleMode("LAND")

	speed = 1
	dur = flight_alt/ speed
	dur = int(dur) + 10
	dur = float(dur)
	time_elapsed_new = InFlight(dur, init_time, abort)
	Record_Data_final(File_name, time_elapsed_new, vehicle, abort)

	time.sleep(5)
	print( "close vehicle object")
	vehicle.close()

def Vehicle_Land_non_log():
	global vehicle
	print ("Landing...")
	vehicle.mode = VehicleMode("LAND")

	time.sleep(5)
	print ("close vehicle object")
	vehicle.close()

def Manual_mode():
	abort = 0
	global vehicle
	vehicle.mode = VehicleMode("ALT_HOLD")
	print ("Vehicle Mode Changed to Fly-Manual...")
	time.sleep(1)
	while True:
		time.sleep(0.01)

def Vehicle_loiter():
	global vehicle
	vehicle.mode = VehicleMode("LOITER")
	print ("Vehicle Mode Changed to Loiter...")
	time.sleep(5)
	while True:
		time.sleep(0.01)

def get_time():
	time.time()

def real_time_plot(lat,lon,alt):
	x = lat
	y = lon
	z = alt
	print("Inside real time plot")
	print("latitude is",x)
	print("longitude is",y)
	print("Altitude is",z)
	figure = plt.figure
	plt.title(" Top View Plot")
	plt.xlabel('x position')
	plt.ylabel('y position')
	#plt.axis(x1,x2,y1,y2)
	plt.plot(x,y,'b-')

	#plt.subplot(112)
	#plt.plot(y,z,'r--')
	plt.show()

def get_bearing(prev_lat,prev_lon,req_lat,req_lon):
    """
    Returns the bearing between the two LocationGlobal objects passed as parameters.

    This method is an approximation, and may not be accurate over large distances and close to the
    earth's poles. It comes from the ArduPilot test code:
    https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
    """
    off_x = req_lon - prev_lon
    off_y = req_lat - prev_lat
    bearing = 90.00 + math.atan2(-off_y, off_x) * 57.2957795
    if bearing < 0:
        bearing += 360.00
    return bearing;

# The original function changed a bit

def setting_yaw():
	print("Inside setting_yaw")
	angle1 = (vehicle.attitude.yaw)*180/3.14
	print("Current angle is", angle1,vehicle.attitude.yaw)

	init_time = time.time()

	Vertical_goto(5,1,1,init_time,abort)

	while True:
		angle2 = (vehicle.attitude.yaw)*180/3.14 - angle1
		print("Correction angle is", angle2)
		if angle2 > 180:
                        angle2 = 180 - angle2
                        print("new correction angle", angle2)

		condition_yaw(-angle2,abort,True)
		'''
		g = open(File_name,"a")
		g.write(str(round(time.time()-init_time,3)))
		g.write("\t")
		g.write(str(vehicle.channels[3]))
		g.write("\t")
		g.write(str(altitude))
		g.write("\t")
		g.write(str(vehicle.channels[2]))
		g.write("\t")
		g.write(str(Pitch))
		g.write("\t")
		g.write(str(vehicle.channels[1]))
		g.write("\t")
		g.write(str(Roll))
		g.write("\t")
		g.write(str(vehicle.channels[4]))
		g.write("\t")
		g.write(str(Yaw))
		g.write("\t")
		g.write(str(vehicle.battery.voltage))
		g.write("\t")
		g.write(str(vehicle.battery.current))
		g.write("\t")
		g.write(str(vehicle.battery.level))
		g.write("\t")
		g.write(str(latitude))
		g.write("\t")
		g.write(str(longitude))
		g.write("\n")
		g.close()
		'''
		print("setting the yaw ")
		if vehicle.attitude.yaw <= 0.95*angle1 or vehicle.attitude.yaw >= 1.05*angle1 :
			print("yaw is set")
			break



#...........Main Code Call Functions.................................#

#........... We dont use the main code in the UI..................
'''
def main_code():
	print("IN main")
	Set_Variables()

	Noctua_initialise()

	arm_and_takeoff(7)

	initialize_time = time.time()
	global_timer = 0

	while abort == 1:
		init_time = time.time()
		Horizontal_goto(12.9882837,80.2283617,7,0.5,init_time,abort)

		init_time = time.time()
		Horizontal_goto(12.9882889,80.2284297,7,0.5,init_time,abort)

		init_time = time.time()
		Horizontal_goto(12.9882216,80.2284291,7,0.5,init_time,abort)

		break

	if check==1:
		Vehicle_loiter()
		#vehicle.mode = VehicleMode("LOITER")
		#print "Vehicle Mode Changed to Loiter..."
		#time.sleep(1)
		#while True:
			#time.sleep(0.01)

	if check_gps==1:
		Manual_mode()
	#	vehicle.mode = VehicleMode("ALT_HOLD")
		#print "Vehicle Mode Changed to Fly-Manual..."
		#time.sleep(1)
		#while True:
			#time.sleep(0.01)

	print "Outside Loop"
	Vehicle_Land()
'''
