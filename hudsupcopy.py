import sys

import math
from dronekit import connect, VehicleMode, LocationGlobalRelative
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import DC_gimbal as backend
from datetime import date
import time

def counterrotate(ox,oy,px,py,roll):

	angle=math.radians(roll)
	#counterclockwise
	qx= ox+ math.cos(angle)*(px-ox)-math.sin(angle)*(py-oy)
	qy= oy+ math.sin(angle)*(px-ox)+math.cos(angle)*(py-oy)
	return qx,qy

def rotate(ox,oy,px,py,roll):

	angle=math.radians(roll)
	#clockwise
	qx= ox+ math.cos(angle)*(px-ox)+math.sin(angle)*(py-oy)
	qy= oy+ math.sin(angle)*(px-ox)+math.cos(angle)*(py-oy)
	return qx,qy

def line_cord(x0,y0,x1,y1,ox,oy,pitch,roll):
	x0,y0 = rotate(ox,oy,x0,y0, roll*5)
	y0 -= pitch*20
	x1, y1 = counterrotate(ox,oy,x1,y1, roll*5)
	y1 -= pitch*20
	return x0,y0,x1,y1

def rec_line_cord(i,list,xc,origin_list,pitch,roll):
	if i!=36:
		list[i*4],list[i*4+1],list[i*4+2],list[i*4+3] = line_cord(list[i*4],list[i*4+1],list[i*4+2],list[i*4+3],xc,origin_list[i],pitch,roll)
		i+=1
		rec_line_cord(i,list,xc,origin_list,pitch,roll) 

def rec_drawLine(self,i,list,painter,roll,metrics):
	if i!=36:

		painter.setPen(QPen(Qt.SolidLine))
		painter.setBrush(QColor("black"))
		if (i==18) or (i==0):
			i+=1
			rec_drawLine(self,i,list,painter,roll,metrics)

		painter.drawLine(list[i*4],list[i*4+1],list[i*4+2],list[i*4+3])
		if (i<18) and (i%2==0):
		  painter.drawText(list[i*4]-15,list[i*4+1]+10,str(-i*5))
		elif (i>18) and (i%2==0):
		  painter.drawText(list[i*4]-15,list[i*4+1]+10,str((i*5)-90))
		
		drawNeedle(self,painter,roll*5)
		i+=1
		rec_drawLine(self,i,list,painter,roll,metrics)


def drawMarkings(self, painter):
		
		rect=QRect(180,50,200,200)
		startAngle = 30 * 16
		arcLength = 180 * 16
		_margins = 10
		_pointText = {0: "0", 10: "10", 20: "20", 30: "30", 40: "45",50: "50", 60: "60", 300: "-60",310: "-50", 320: "-40", 330: "-30", 340: "-20", 350: "-10"}
		painter.save()
		painter.translate(self.width()/2, self.height()/2)
		scale = min((self.width() - _margins)/120.0,(self.height() - _margins)/120.0)
		painter.scale(scale, scale)
		 
		font = QFont(self.font())
		font.setPixelSize(5)
		metrics = QFontMetricsF(font)
		 
		painter.setFont(font)
		painter.setPen(self.palette().color(QPalette.Shadow))
		 
		i = 0
		while i < 360:
		 
		  if (i % 10 == 0) and ((i <=60) or (i >=300 and i<360)):
			  painter.drawLine(0, -35, 0, -40)
			  painter.drawText(-metrics.width(_pointText[i])/2.0, -42,_pointText[i])

		  painter.rotate(10)
		  i += 10
		 
		painter.restore()

def drawNeedle(self, painter,roll):

		painter.save()
		painter.translate(self.width()/2, self.height()/2)
		painter.rotate(roll)
		scale = min((self.width() - self._margins)/120.0,(self.height() - self._margins)/120.0)
		painter.scale(scale, scale)
		 
		painter.setPen(QPen(Qt.NoPen))
		painter.setBrush(self.palette().brush(QPalette.Shadow))
		 
		# painter.drawPolygon(QPolygon([QPoint(-10, 0), QPoint(0, -45), QPoint(10, 0),QPoint(0, 45), QPoint(-10, 0)]))
		 
		painter.setBrush(self.palette().brush(QPalette.Highlight))
		 
		painter.drawPolygon(QPolygon([QPoint(-5, -15), QPoint(0, -35), QPoint(5, -15),QPoint(0, -20), QPoint(-5, -15)]))
		 
		painter.restore()

def Compass(self,painter,yaw,r):
	i = r.center().x()
	# print(yaw)
	yaw_pix = int(yaw*6)
	pos_pix = yaw_pix + r.center().x()
	deg_pix = 0   
	
	if(yaw<0):
		yaw+=360
	
	painter.drawLine(r.center().x(),10,r.center().x(),35)
	painter.drawText(r.center().x()-10,50,str(int(yaw)))
	
	if (pos_pix <= r.right()):
		shift = r.center().x() - pos_pix
	else:
		pos_pix = r.center().x()-(pos_pix-r.center().x())
		shift = r.center().x() - pos_pix
	if(shift >= 0):
		while (i <= r.right()+shift):
			j = r.center().x()-(i-r.center().x())
			if (i <= r.right()+shift) and ((i-r.center().x())%90 == 0):
				angle = deg_pix/6
				painter.drawLine(i+shift,15,i+shift,30)
				painter.drawText(i+shift,15,str(int(angle)))
				if (i != j):
					painter.drawLine(j+shift,15,j+shift,30)
					painter.drawText(j+shift,15,str(int(360-angle)))
				
			painter.drawLine(i+shift,20,i+shift,30)
			painter.drawLine(j+shift,20,j+shift,30)
			i+=30
			deg_pix+=30
	else:
		while (i <= r.right()-shift):
			j = r.center().x()-(i-r.center().x())
			if (i <= r.right()-shift) and ((i-r.center().x())%90 == 0):
				angle = deg_pix/6
				painter.drawLine(i+shift,15,i+shift,30)
				painter.drawText(i+shift,15,str(int(angle)))
				if (i != j):
					painter.drawLine(j+shift,15,j+shift,30)
					painter.drawText(j+shift,15,str(int(360-angle)))
				
			painter.drawLine(i+shift,20,i+shift,30)
			painter.drawLine(j+shift,20,j+shift,30)
			i+=30
			deg_pix+=30



class Manager(QObject):
	changedValue = pyqtSignal(tuple)
	def __init__(self):
		QObject.__init__(self)
		# abort = 1
		# vehicle= backend.Noctua_initialise()
		filename = "Attitude_data_Manual_1232018_1158.txt"
		# abort,filename = backend.Set_Variables()
		# res= backend.vehicle.attitude.pitch,
		# res = backend.Record_Data(filename,backend.vehicle,abort)
		res = self.read_content(filename)

		self.results = zip(*res)

		self.timer = QTimer(self)
		self.timer.timeout.connect(self.update_value)
		self.timer.start(50)

	@pyqtSlot()
	def update_value(self):
		try:
			self.changedValue.emit(next(self.results))
		except StopIteration:
			self.timer.stop()
	def read_content(self, filename):
		pitch = []
		roll = []
		yaw = []
		with open(filename,'r') as f :
			lines = f.readlines()[4:]
			for s in lines:
				a = s.split()
				if len(a) !=14:
					print("point reached")
					continue
				pitch.append(float(a[4]))
				roll.append(float(a[6]))
				yaw.append(float(a[8]))
		return pitch, roll, yaw



#     last_attitude_cache = None
# def attitude_callback(self, attr_name, value):
#     # `attr_name` - the observed attribute (used if callback is used for multiple attributes)
#     # `self` - the associated vehicle object (used if a callback is different for multiple vehicles)
#     # `value` is the updated attribute value.
#     global last_attitude_cache
#     abort,filename = backend.Set_Variables()
#     res = backend.Record_Data(filename,backend.vehicle,abort)
#     # Only publish when value changes
#     if value!=last_attitude_cache:
#         print " CALLBACK: Attitude changed to", value
#         last_attitude_cache=value

# # print "\nAdd `attitude` attribute callback/observer on `vehicle`"     
# vehicle.add_attribute_listener('attitude', attitude_callback)

# # print " Wait 2s so callback invoked before observer removed"
# time.sleep(1)

# # print " Remove Vehicle.attitude observer"    
# # Remove observer added with `add_attribute_listener()` specifying the attribute and callback function
# vehicle.remove_attribute_listener('attitude', attitude_callback)


class Widget(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.v = None

	@pyqtSlot(tuple)
	def update_value(self, v):
		self.v = v
		self.update()

	def paintEvent(self, event):
		QWidget.paintEvent(self, event)
		painter = QPainter(self)
		r = self.rect()
		font = QFont(self.font())
		font.setPixelSize(15)
		metrics = QFontMetricsF(font) 
		painter.setFont(font)
		painter.setPen(self.palette().color(QPalette.Shadow))

		#updating values of vehicle in hudsup display
		# last_attitude_cache = None
		# global res
		# def attitude_callback(self, attr_name, value):
		#     global last_attitude_cache
		#     abort,filename = backend.Set_Variables()
		#     res = backend.Record_Data(filename,backend.vehicle,abort)
		#     if value!=last_attitude_cache:
		#         print " CALLBACK: Attitude changed to", value
		#         last_attitude_cache=value



		rect=QRect(180,50,200,200)
		startAngle = 30 * 16
		arcLength = 180 * 16
		self._margins = 10
		self._pointText = {0: "0", 10: "10", 20: "20", 30: "30", 40: "45",50: "50", 60: "60", 300: "-60",310: "-50", 320: "-40", 330: "-30", 340: "-20", 350: "-10"}
		


		if self.v:
		# if backend.vehicle.attitude :
			# x0, x1, y0, y1 = -400, 880, 280, 280
			i=0
			xc=r.center().x()
			yc= r.center().y()

			# center point and endpoints of pitch lines
			list=[]
			origin_list=[]
			for i in range(0,18):
				if i%2==0 :
					list.append(r.center().x()-100)
					list.append(r.center().y()+50*i)
					list.append(r.center().x()+100)
					list.append(r.center().y()+50*i)
					origin_list.append(yc+50*i+50)
				else :
					list.append(r.center().x()-50)
					list.append(r.center().y()+50*i)
					list.append(r.center().x()+50)
					list.append(r.center().y()+50*i)
					origin_list.append(yc+50*i+50)

			for i in range(0,18):
				if i%2==0:
					list.append(r.center().x()-100)
					list.append(r.center().y()-50*i)
					list.append(r.center().x()+100)
					list.append(r.center().y()-50*i)
					origin_list.append(yc-50*i+50)
				else :
					list.append(r.center().x()-50)
					list.append(r.center().y()-50*i)
					list.append(r.center().x()+50)
					list.append(r.center().y()-50*i)
					origin_list.append(yc-50*i+50)

			pitch, roll, yaw = self.v
			count=0

			x0,y0,x1,y1 = line_cord(r.left()-200, r.center().y(), r.right()+200, r.center().y(),r.center().x(),r.center().y(),pitch,roll)
			rec_line_cord(count,list,r.center().x(),origin_list,pitch,roll)

			posx0 = QPoint(x0,y0)
			posx1 = QPoint(x1, y1)

			upper_polygon = QPolygonF([r.topLeft(), posx0, posx1, r.topRight()])
			bottom_polygon = QPolygonF([posx0, posx1, r.bottomRight(), r.bottomLeft()])


			painter.setBrush(QColor("skyblue"))
			painter.drawPolygon(upper_polygon)

			painter.setBrush(QColor("green"))
			painter.drawPolygon(bottom_polygon)

			rec_drawLine(self,count,list,painter,roll,metrics)
			Compass(self,painter,yaw,r)

		painter.setPen(QPen(Qt.SolidLine))
		painter.setBrush(QColor("Red"))   
		# painter.drawArc(rect,startAngle,arcLength)
		drawMarkings(self,painter)
		painter.drawLine(r.left(), r.center().y(), r.right(), r.center().y())
		painter.drawLine(r.left(),25,r.right(),25)
		painter.setPen(QPen(Qt.SolidLine))
		painter.drawLine(r.center().x(),35,r.center().x()-10,40)
		# drawRect (r.center().x()-10,25, 20,10)
		# painter.drawLine(r.center().x(),20,r.center().x(),30)
		# painter.drawText(r.center().x(),15,'1')


		# vehicle.add_attribute_listener('backend.vehicle.attitude', attitude_callback)
		# time.sleep(1)
		# vehicle.remove_attribute_listener('backend.vehicle.attitude', attitude_callback)

def main():
	# app = QApplication(sys.argv)
	manager = Manager()
	w = Widget()
	manager.changedValue.connect(w.update_value)
	w.show()
	# sys.exit(app.exec_())
if __name__ == '__main__':

	  app = QApplication(sys.argv)
	  main()
	  # # backend.Noctua_initialise()
	  # manager = Manager()
	  # huds_Up()
	  # # w = Widget()
	  # # manager.changedValue.connect(w.update_value)
	  # # w.show()
	  sys.exit(app.exec_())